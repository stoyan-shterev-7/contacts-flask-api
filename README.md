##Clone
`git clone https://stoyan-shterev-7@bitbucket.org/stoyan-shterev-7/contacts-flask-api.git`

##Virtual Environment
`python3 -m venv contacts_env`

Activate this environment as follows:
`source/bin/activate` for Linux distributions,
`cd contacts_env/Scripts` and run `activate` for Windows

##Install dependencies Using PIP in the virtual environment

`pip install -r requirements.txt`

##Run project
`flask run `

##Run tests

`python tests.py`