from flask import request
from app import app
from app.resources.contacts import get_all_contacts, get_contact, delete_contact, create_contact, update_contact, \
    get_all_emails, get_email, create_email, update_email, delete_email


# Retrieve and Create operations for /contacts endpoint.
@app.route('/contacts', methods=['GET', 'POST'])
def contacts():
    if request.method == 'GET':
        return get_all_contacts()
    elif request.method == 'POST':
        return create_contact()


# Retrieve,Delete,Update operations for /contacts/<username> endpoint.
@app.route('/contacts/<username>', methods=['GET', 'DELETE', 'PUT'])
def contact_by_name(username):
    if request.method == 'GET':
        return get_contact(username)
    elif request.method == 'DELETE':
        return delete_contact(username)

    else:
        return update_contact(username, request)


# Retrieve and Create operations for /emails endpoint.
@app.route('/emails', methods=['GET', 'POST'])
def emails():
    if request.method == 'GET':
        return get_all_emails()
    elif request.method == 'POST':
        return create_email()


# Retrieve, Delete, Update operations for /emails/<id> endpoint.
@app.route('/emails/<id>', methods=['GET', 'DELETE', 'PUT'])
def email_by_id(id):
    if request.method == "GET":
        return get_email(id)
    elif request.method == "DELETE":
        return delete_email(id)
    else:
        return update_email(id)
