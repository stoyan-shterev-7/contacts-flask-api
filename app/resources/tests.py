import os
from flask import Flask
import unittest
import tempfile

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class FlaskTestCase(unittest.TestCase):

    def setUp(self):
        print("Testing: Setup App Data Base ")

        # Temporal URI for the testing database
        temp_database_uri = 'sqlite:////tmp/flask_unittest.db'

        # Database Debug (verbosity for db operations on console)
        database_debug = False  # 'debug'

        # Setup temporal database on flask (overwrite flask app database with temporal one)
        Base.engine = create_engine(temp_database_uri,
                                    convert_unicode=True,
                                    echo=database_debug)
        Base.DBSession = sessionmaker(autocommit=False,
                                      autoflush=False,
                                      bind=Base.engine)

        # Setup db session
        Base.db_session = scoped_session(Base.DBSession)

        # Always dropall previous data before testing on temporal database
        Base.metadata.drop_all(bind=Base.engine)

        # Setup app for testing
        Base.app.testing = True

        # Setup app test client
        self.app = Base.app.test_client()
        with Base.app.app_context():
            # initialize database
            Base.setup()

    def tearDown(self):
        print("Testing: Clossing down database")
        # flask.db_session.remove()

    def test_unavailable_resource(self):
        print("Testing: POST method non supported")
        rv = self.app.get('/')
        assert rv._status == '404 NOT FOUND'
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Resource Not Found or Not Available."

        print("Testing: POST method non supported")
        rv = self.app.get('/contacts/nonavailable/resource')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Resource Not Found or Not Available."

    def test_method_not_allowed(self):
        print("Testing: Method not allowed")

        rv = self.app.post('/contacts/stoyan',
                           json={"username": "stoyan",
                                 "email": "stoyan.shterev96@gmail.com",
                                 "firstname": "Stoyan",
                                 "surname": "Shterev"})
        assert rv.get_json()['status'] == 405
        assert rv.get_json()["message"] == "HTTP Method was Not Allowed for the Request."

    def test_post(self):
        print("Testing: POST method new username")
        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": "sto1@gmail.com",
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        assert rv.get_json()['message'] == "Contact inserted succesfully in the database. ( Username = sto1 )"
        assert rv.get_json()['status'] == 200

        print("Testing: POST when null argument")
        rv = self.app.post('/contacts/',
                           json={"username": "jsnow",
                                 "email": "jon.snow@gmail.com",
                                 "firstname": "Jon"})

        assert rv.get_json()['status'] == 400
        assert rv.get_json()['message'] == "Bad Request. Invalid Keys in Data JSON request."

        print("Testing: POST method existing username")
        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": ["sto1@gmail.com",
                                           "sto1@hotmail.com"],
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        # print("hello ->",rv.get_json()['message'])
        assert rv.get_json()['message'] == "Integrity Error due to Duplicated Index or Null Key was provided."
        assert rv.get_json()['status'] == 400

        print("Testing: POST new user with multiple emails")
        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": ["sto1@gmail.com", "sto1@hotmail.com"],
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        assert rv.get_json()['message'] == "Contact inserted succesfully in the database. ( Username = sto1 )"

        assert rv.get_json()['status'] == 200

    def test_get_by_username(self):
        print("Testing: GET contact by username")

        # This test requires POST method works
        rv = self.app.post('/contacts/',
                           json={"username": "sto12345",
                                 "email": ["sto12345@gmail.com", "sto12345@hotmail.com"],
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        rv = self.app.get('/contacts/sto12345')
        assert rv.get_json()['status'] == 200
        assert rv.get_json()['message'] == "Specific Contact retrieved succesfully from the database."

        print("Testing: Get contact by a missing username")
        rv = self.app.get('/contacts/stoffffffffff1')
        assert rv.get_json()['status'] == 404

        assert rv.get_json()['message'] == "Contact Not Found in database. Username 'stoffffffffff1' does not exist."

    def test_get_all(self):
        print("Testing: Get All Contacts")

        # Insert a contact to have something in the database
        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": ["sto1@gmail.com",
                                           "sto1@hotmail.com"],
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        rv = self.app.post('/contacts/',
                           json={"username": "jon.snow",
                                 "email": ["jon.snow@gmail.com",
                                           "jon.snow@hotmail.com"],
                                 "firstname": "Jon",
                                 "surname": "Snow"})

        rv = self.app.get('/contacts/')
        json_data = rv.get_json()

        assert json_data["contacts"][0]["username"] == 'sto1'
        assert json_data["contacts"][0]["email"] == ["sto1@gmail.com",
                                                     "sto1@hotmail.com"]
        assert json_data["contacts"][0]["firstname"] == 'Sto'
        assert json_data["contacts"][0]["surname"] == 'Stones'

        assert json_data["contacts"][1]["username"] == 'jon.snow'
        assert json_data["contacts"][1]["email"] == ['jon.snow@gmail.com',
                                                     'jon.snow@hotmail.com']
        assert json_data["contacts"][1]["firstname"] == 'Jon'
        assert json_data["contacts"][1]["surname"] == 'Snow'

        assert rv.get_json()['status'] == 200

    def test_get_contacts_by_email(self):
        print("Testing: Get contacts by email")

        # Insert contacts to have something in the database
        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": ["sto1@gmail.com",
                                           "sto1@hotmail.com",
                                           "sto2@gmail.com"],
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        rv = self.app.post('/contacts/',
                           json={"username": "sto2",
                                 "email": ["sto20@gmail.com",
                                           "sto20@hotmail.com",
                                           "sto30@gmail.com"],
                                 "firstname": "Stos",
                                 "surname": "Stones"})

        rv = self.app.post('/contacts/',
                           json={"username": "jim.johnson",
                                 "email": ["jjohnson@gmail.com",
                                           "jjohnson@hotmail.com"],
                                 "firstname": "Jim",
                                 "surname": "Johnson"})

    def test_delete_user(self):
        print("Testing: Delete a User")

        rv = self.app.post('/contacts/',
                           json={"username": "sto1",
                                 "email": "sto1@gmail.com",
                                 "firstname": "Sto",
                                 "surname": "Stones"})

        rv = self.app.post('/contacts/',
                           json={"username": "stoyan",
                                 "email": ["stoyan.shterev96@gmail.com",
                                           "sto1@hotmail.com"],
                                 "firstname": "Stoyan",
                                 "surname": "Shterev"})

        rv = self.app.post('/contacts/',
                           json={"username": "jon.snow",
                                 "email": ["jon.snow@gmail.com",
                                           "jon.snow@hotmail.com"],
                                 "firstname": "Jon",
                                 "surname": "Snow"})

        # Try to delete again
        print("Testing: Delete already deleted contacts")

        rv = self.app.delete('/contacts/sto1')
        assert rv.get_json()['status'] == 200
        assert rv.get_json()["message"] == "Contact deleted from the database."

        rv = self.app.delete('/contacts/stoyan')
        assert rv.get_json()['status'] == 200
        assert rv.get_json()["message"] == "Contact deleted from the database."

        # Try to get deleted contacts
        print("Testing: Get deleted contact by username")
        rv = self.app.get('/contacts/sto1')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()["message"] == "Contact Not Found in database. Username 'sto1' does not exist."
        rv = self.app.delete('/contacts/stoyan')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()["message"] == "Contact Not Found in database. Username 'stoyan' does not exist."

        # Check that usernames deleted does not exist in the database trying to get them
        rv = self.app.get('/contacts/sto1')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Contact Not Found in database. Username 'sto1' does not exist."

        rv = self.app.get('/contacts/stoyan')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Contact Not Found in database. Username 'stoyan' does not exist."

        rv = self.app.get('/contacts/jon.snow')
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Contact Not Found in database. Username 'jon.snow' does not exist."

    def test_update_user(self):
        print("Testing: Updated a user with multiple emails")

        rv = self.app.post('/contacts/',
                           json={"username": "jsnow",
                                 "email": ["jon.snow@gmail.com",
                                           "jon.snow@hotmail.com"],
                                 "firstname": "Jon",
                                 "surname": "Snow"})

        rv = self.app.put('/contacts/jsnow',
                          json={"email": ["jsnow_updated@gmail.com",
                                          "jsnow_updated@hotmail.com",
                                          "new_updated_abeation@hotmail.com"],
                                "firstname": "John",
                                "surname": "Snow"})

        rv = self.app.get('/contacts/jsnow')
        assert rv.get_json()['status'] == 200
        assert rv.get_json()['message'] == 'Specific Contact retrieved succesfully from the database.'
        assert rv.get_json()['contact']['email'] == ['jsnow_updated@gmail.com',
                                                     'jsnow_updated@hotmail.com',
                                                     'new_jsnow@hotmail.com']
        assert rv.get_json()['contact']['firstname'] == 'John'

        print("Testing: Updated a missing user")

        rv = self.app.put('/contacts/sto12345',
                          json={"email": ["sto_updated@gmail.com",
                                          "sto_updated@hotmail.com"],
                                "firstname": "Sto",
                                "surname": "Stones"})
        assert rv.get_json()['status'] == 404
        assert rv.get_json()['message'] == "Contact Not Found in database. Username 'sto12345' does not exist."


if __name__ == '__main__':
    unittest.main()
