from app.resources.models import Contact, db, Email
from json import dumps, loads
from sqlalchemy import inspect
from flask import Flask, request


contact_mapper = inspect(Contact)
email_mapper = inspect(Email)


def get_all_contacts():
    all_contacts = Contact.query.all()
    contact_list = []
    for contact in all_contacts:
        attributes = []
        for c in contact_mapper.attrs:
            attribute = getattr(contact, c.key)
            attributes.append(
                [c.key, [str(entry) for entry in attribute] if isinstance(attribute, list) else attribute])
        contact_list.append(dict(attributes))
    return dumps(contact_list)


def get_all_emails():
    all_emails = Email.query.all()
    email_list = []
    for email in all_emails:
        email_list.append(dict([(e.key, str(getattr(email, e.key))) for e in email_mapper.attrs]))
    return dumps(email_list)


def get_email(id):
    email = Email.query.filter_by(id=id).first()

    attributes = []
    for e in email_mapper.attrs:
        attribute = getattr(email, e.key)
        attributes.append(
            [e.key, [str(entry) for entry in attribute] if isinstance(attribute, list) else attribute])
    return dumps(dict(attributes))


def create_email():
    data = request.get_json()
    new_email = Email(id=data['id'], email=data['email'], username=data['username'])
    db.session.add(new_email)
    db.session.commit()
    return '', 201


def update_email(id):
    modified_email = Email.query.get(id)

    email = request.json['email']
    username = request.json['username']

    modified_email.email = email
    modified_email.username = username

    db.session.commit()
    return '', 204


def delete_email(id):
    Email.query.filter_by(id=id).delete()
    db.session.commit()
    return '', 204


def get_contact(user_ref):
    contact = Contact.query.filter_by(username=user_ref).first()
    if not contact:
        user_email = Email.query.filter_by(email=user_ref).first()
        contact = Contact.query.filter_by(username=user_email.username).first()
        if not contact:
            return dumps({'error': 'username %s not found' % user_ref}), 404
    attributes = []
    for c in contact_mapper.attrs:
        attribute = getattr(contact, c.key)
        attributes.append(
            [c.key, [str(entry) for entry in attribute] if isinstance(attribute, list) else attribute])
    return dumps(dict(attributes))


def create_contact():
    data = request.get_json()
    new_contact = Contact(username=data['username'], firstname=data['firstname'], surname=data['surname'])
    db.session.add(new_contact)
    db.session.commit()

    return '', 201


def delete_contact(username):
    Contact.query.filter_by(username=username).delete()
    Email.query.filter_by(username=username).delete()
    db.session.commit()

    return '', 204


def update_contact(username, request):
    contact = Contact.query.filter_by(username=username).first()
    request_body = request.form if request.form else loads(request.data)
    for key in request_body:
        try:
            setattr(contact, key, request_body[key])
        except AttributeError:
            new_emails = set(e for e in request_body[key])
            current_emails = set(str(e) for e in Email.query.filter_by(username=username))
            for email in [e for e in new_emails if e not in current_emails]:
                contact.emails.append(Email(email=email, username=username))
            for email in [e for e in current_emails if e not in new_emails]:
                contact.emails.remove(email)
    db.session.commit()
    return '', 204

