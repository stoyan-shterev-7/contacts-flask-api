from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app import app

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Contact(db.Model):
    __tablename__ = 'contacts'
    username = db.Column(db.String, primary_key=True)
    firstname = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    emails = db.relationship('Email', cascade='all, delete-orphan')

    def __repr__(self):
        return '<Contact %r>' % self.username


class Email(db.Model):
    __tablename__ = 'emails'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)
    username = db.Column(db.String, db.ForeignKey('contacts.username'))

    def __repr__(self):
        return self.email
